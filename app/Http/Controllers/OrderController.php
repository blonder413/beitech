<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\StoreOrder;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Order::latest('creation_date')->paginate(10);
        return view('order.index', [
            "model" => $model,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Order;
        $customers = Customer::all();

        return view('order.create', [
            'customers' => $customers,
            'model' => $model,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrder $request)
    {
        /*
        $atributos = request()->validate([
            'customer_id'       => 'required',
            'creation_date'     => 'required',
            'delivery_address'  => 'required',
            'total'             => 'required',
        ]);
        */

//        $atributos['usuariocrea'] = auth()->id();
//        $atributos['usuariomodifica'] = auth()->id();

        Order::create([
            'customer_id'   => $request->customer_id,
            'creation_date' => $request->creation_date,
            'delivery_address'  => $request->delivery_address,
            'total'             => $request->total,
            'usuariocrea'       => aut()->id(),
            'usuariomodifica'   => aut()->id(),
        ]);

//        Order::create(request->all());
//        session()->flash('success', 'Registro creado exitosamente');
        return redirect('order')->with('success', 'Orden registrada exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
//        dd($order);
        $customers = Customer::all();
        //$model = Order::find($order);

        return view('order.edit', [
            'customers' => $customers,
            'model' => $order,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $atributos = request()->validate([
            'customer_id'       => 'required',
            'creation_date'     => 'required',
            'delivery_address'  => 'required',
            'total'             => 'required',
        ]);
        $atributos['usuariomodifica'] = auth()->id();

        Order::update($atributos);

//        session()->flash('success', 'Registro actualizado exitosamente');
        return redirect('order')->with('success', 'Orden registrada exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect('order')->with('success', 'Registro eliminado exitosamente');
    }
}
