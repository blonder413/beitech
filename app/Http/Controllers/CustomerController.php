<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Customer::paginate(10);
        return view('customer.index', [
            "model" => $model,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Customer;
        return view('customer.create', [
            'model' => $model,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $datos = request()->all();
//        ddd($datos);

        $atributos = request()->validate([
            'name'  => 'required',
            'email'  => 'required|email',
        ]);

//        $model = request()->except('_token');
//        Customer::insert($model);
        Customer::create($atributos);
        return redirect('customer')->with('mensaje', 'Cliente registrado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
//    public function edit(Customer $customer)
    {
        $model = Customer::findOrFail($id);
        return view('customer.edit', [
            'model' => $model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
//        $model = request()->all();
        $model = request()->except(['_token', '_method']);
        Customer::where('id', '=', $customer)->update($model);
        return redirect('customer')->with('mensaje', 'Cliente actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        Customer::destroy($customer);
        return redirect('customer')->with('mensaje', 'Cliente eliminado exitosamente');
    }
}
