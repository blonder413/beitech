<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id'       => 'required',
            'creation_date'     => 'required',
            'delivery_address'  => 'required',
            'total'             => 'required',
        ];
    }

    /**
     * Nombres de los atributos para los mensajes de error
     * @return array
     */
    public function attributes()
    {
        return [
            'delivery_address'  => 'Dirección',
            'creation_date'     => 'Fecha de creación',
        ];
    }

    /**
     * Mensajes personalizados para los mensajes de error
     * @return array
     */
    public function messages()
    {
        return [
            'total.required'  => 'Debe poner un valor total',
        ];
    }
}
