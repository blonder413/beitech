<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Customer;

class Order extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'order';
    protected $primaryKey = 'order_id';
    protected $fillable = ['customer_id', 'creation_date', 'delivery_address', 'total'];
//    protected $guarded = [];

    public function customer()
    {
//        return $this->hasOne(Customer::class, 'customer_id', 'customer_id');
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }
}

