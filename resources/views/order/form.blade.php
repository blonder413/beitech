<div class="form-group">
    <label for="customer_id">Customer</label>
    <select name="customer_id" id="customer_id" class='form-control'>
    @foreach($customers as $value)
        <option value="{{ $value->customer_id }}">{{ old('name', $value->customer_id) }}</option>
    @endforeach
    </select>
    @error('customer_id')
    <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
    @enderror
</div>

<div class="form-group">
    <label for="creation_date">Fecha</label>
    <input type="text" name="creation_date" placeholder="Fecha" value="{{ old('creation_date',$model->creation_date) }}" class='form-control'>
    @error('creation_date')
    <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
    @enderror
</div>
<div class="form-group">
    <label for="delivery_address">Dirección</label>
    <input type="text" name="delivery_address" placeholder="Direccion" value="{{ old('delivery_address', $model->delivery_address) }}" class='form-control'>
    @error('delivery_address')
    <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
    @enderror
</div>
<div class="form-group">
    <label for="total">Total</label>
    <input type="text" name="total" placeholder="Total" value="{{ old('total', $model->total) }}" class='form-control'>
    @error('total')
    <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
    @enderror
</div>
<div class="form-group">
    <input type="submit" value="Enviar" name="enviar" class='form-control btn btn-success'>
</div>
