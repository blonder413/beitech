@extends('layouts.app')

@section('content')

<h1>Crear Orden</h1>

    <form action="{{ url('order') }}" method="POST">
        @csrf
        @include('order.form', ['model' => $model])
    </form>
@endsection
