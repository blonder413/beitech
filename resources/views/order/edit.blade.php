@extends('layouts.app')

@section('content')

<h1>Editar Orden</h1>

    <form action="{{ route('order.update', $model) }}" method="POST">
        @csrf
        @method('PATCH')
        @include('order.form', ['model' => $model])
    </form>

@endsection