@extends('layouts.app')

@section('content')

<h1>Ordenes</h1>

<a href="{{ url('/order/create') }}" class="btn btn-success">Crear</a>

<table class='table table-hover'>

    <tr>
        <th>Order id</th>
        <th>Curstomer id</th>
        <th>creation date</th>
        <th>delivery address</th>
        <th>total</th>
        <th></th>
    </tr>

    @foreach ($model as $value)
    <tr>
        <td>{{ $value->order_id }}</td>
        <td>{{ $value->customer->name }}</td>
        <td>{{ $value->creation_date }}</td>
        <td>{{ $value->delivery_address }}</td>
        <td>{{ $value->total }}</td>
        <td>
            <a href="{{ route('order.edit', $value) }}" class='btn btn-primary'>Editar</a>
            <form action="{{ route('order.destroy', $value) }}" method="post">
                @csrf
                {{ method_field('DELETE') }}
                <input type="submit" value="Eliminar" onclick="return confirm('Realmente desea eliminar este registro?')" class='btn btn-danger'>
            </form>
    </tr>
    @endforeach
</table>

<?= $model->links(); ?>


@endsection
