<div class="form-group">
    <input type="text" name="customer_id" placeholder="Customer" value="{{ $model->customer_id }}" class='form-control'>
</div>
<div class="form-group">
    <input type="text" name="name" placeholder="Nombre" value="{{ $model->name }}" class='form-control'>
</div>
<div class="form-group">
    <input type="text" name="email" placeholder="Correo" value="{{ $model->email }}" class='form-control'>
</div>
<div class="form-group">
    <input type="submit" value="Enviar" name="enviar" class='form-control btn btn-success'>
</div>