<h1>Editar Cliente</h1>

<form action="{{ url('/customer/' . $model->id) }}" method="post">
    @csrf
    {{ method_field('PATCH') }}
    @include('customer.form')
</form>