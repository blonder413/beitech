@extends('layouts.app')

@section('content')

<h1>Crear Cliente</h1>

    <form action="{{ url('/customer') }}" method="post">
        @csrf
        @include('customer.form')
    </form>
@endsection
