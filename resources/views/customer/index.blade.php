@extends('layouts.app')

@section('content')
<h1>Listado de Clientes</h1>

@if(Session::has('mensaje'))
    <div class='alert alert-success alert-dismissible' role='alert'>
        {{ Session::get('mensaje') }}
    </div>
@endif

<a href="{{ url('customer/create') }}" class='btn btn-primary'>Crear</a>

<table class='table table-hover'>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($model as $value)
        <tr>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td>

                <a href="{{ url('customer/' . $value->customer_id . '/edit') }}">Editar</a>

                <form action="{{ url('/customer/' . $value->customer_id) }}" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="submit" value="Eliminar" onclick="return confirm('Realmente desea eliminar este registro?')" class='btn btn-danger'>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
